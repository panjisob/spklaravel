<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subkriteria;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class SubKriteriaController extends Controller
{
    public function index()
    {
        /// mengambil data terakhir dan pagination 5 list
        $listSubKriteria = Subkriteria::all()->sortBy('kode_subkriteria');
        /// mengirimkan variabel $kriteria ke halaman views kriteria/index.blade.php
        /// include dengan number index
        return view('subkriteria.index',compact('listSubKriteria'));
            // ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subkriteria.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /// membuat validasi untuk title dan content wajib diisi
        $request->validate([
            'kode_subkriteria' => 'required',
            'nama_subkriteria' => 'required',
            'kode_kriteria' => 'required',
        ]);
        echo $request;
        /// insert setiap request dari form ke dalam database via model
        /// jika menggunakan metode ini, maka nama field dan nama form harus sama
        Subkriteria::create($request->all());
         
        /// redirect jika sukses menyimpan data
        return redirect()->route('subkriteria.index')
                        ->with('success','Kriteria created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('SubKriterias.show',$SubKriteria->id) }}
        return view('subkriteria.show',compact('kriteria'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($kode_subkriteria)
    {
        $data = Subkriteria::firstOrFail()->where('kode_subkriteria',$kode_subkriteria)->get();
        $subkriteria = $data[0];
        return view('subkriteria.edit',compact('subkriteria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kode_subkriteria)
    {
        /// membuat validasi untuk title dan content wajib diisi
        $request->validate([
            'kode_subkriteria' => 'required',
            'nama_subkriteria' => 'required',
            'tipe' => 'required',
            'kode_kriteria' => 'required',
        ]);

        /// mengubah data berdasarkan request dan parameter yang dikirimkan
        DB::table('subkriterias')->where('kode_subkriteria',$kode_subkriteria)->update($request->except(['_token','_method']));
         
        /// setelah berhasil mengubah data
        return redirect()->route('subkriteria.index')
                        ->with('success','SubKriteria updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode_subkriteria)
    {
        /// melakukan hapus data berdasarkan parameter yang dikirimkan
        Subkriteria::firstOrFail()->where('kode_subkriteria',$kode_subkriteria)->delete();
        return redirect()->route('subkriteria.index')
                        ->with('success','SubKriteria deleted successfully');
    }
}
