<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mahasiswa;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        $listMahasiswa = Mahasiswa::where('user_id', Auth::user()->id)->get();
        return view('home', compact('listMahasiswa'));
    }
}
