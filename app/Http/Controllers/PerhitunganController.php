<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Subkriteria;
use App\Models\Kriteria;
use App\Models\Mahasiswa;

class PerhitunganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nilai = DB::table('nilais')
        ->select('nilais.nim','nilais.nilai',  
            DB::raw("(select subkriterias.nama_subkriteria from subkriterias 
                    where subkriterias.kode_subkriteria = nilais.kode_subkriteria) as nama_subkriteria"),
            DB::raw("nilais.nilai - (select subkriterias.bobot from subkriterias 
                    where subkriterias.kode_subkriteria = nilais.kode_subkriteria) as nilai_bobot"),
            DB::raw("(select subkriterias.kode_kriteria from subkriterias 
                    where subkriterias.kode_subkriteria = nilais.kode_subkriteria) as kode_kriteria"),
            DB::raw("(select subkriterias.tipe from subkriterias 
                    where subkriterias.kode_subkriteria = nilais.kode_subkriteria) as tipe"))
        ->get();

        // echo $nilai;
        $listByKriteria = [];

        foreach ($nilai as $data) {
            $bobotByKriteria[$data->kode_kriteria][] = $data;
            
        }

        // var_dump($bobotByKriteria[1]);
        // echo "<br>";

        //perhitungan untuk pemetaan GAP
        $bobotByNim = [];
        $listKriteria = Kriteria::all();
        foreach ($listKriteria as $krteria){
            foreach ($bobotByKriteria[$krteria->kode_kriteria] as $data){
                if ($data->tipe == 'Secondary Factor'){
                    if ($data->nilai_bobot == 0)
                        $bobotByNim[$krteria->kode_kriteria][$data->nim]['Secondary Factor'][] = 5;
                    if ($data->nilai_bobot == 1)
                        $bobotByNim[$krteria->kode_kriteria][$data->nim]['Secondary Factor'][] = 4.5;
                    if ($data->nilai_bobot == -1)
                        $bobotByNim[$krteria->kode_kriteria][$data->nim]['Secondary Factor'][] = 4;
                    if ($data->nilai_bobot == 2)
                        $bobotByNim[$krteria->kode_kriteria][$data->nim]['Secondary Factor'][] = 3.5;
                    if ($data->nilai_bobot == -2)
                        $bobotByNim[$krteria->kode_kriteria][$data->nim]['Secondary Factor'][] = 3;
                    if ($data->nilai_bobot == 3)
                        $bobotByNim[$krteria->kode_kriteria][$data->nim]['Secondary Factor'][] = 2.5;
                    if ($data->nilai_bobot == -3)
                        $bobotByNim[$krteria->kode_kriteria][$data->nim]['Secondary Factor'][] = 2;
                    if ($data->nilai_bobot == 4)
                        $bobotByNim[$krteria->kode_kriteria][$data->nim]['Secondary Factor'][] = 1.4;
                    if ($data->nilai_bobot == -4)
                        $bobotByNim[$krteria->kode_kriteria][$data->nim]['Secondary Factor'][] = 1;
                } else {
                    if ($data->nilai_bobot == 0)
                        $bobotByNim[$krteria->kode_kriteria][$data->nim]['Core Factor'][] = 5;
                    if ($data->nilai_bobot == 1)
                        $bobotByNim[$krteria->kode_kriteria][$data->nim]['Core Factor'][] = 4.5;
                    if ($data->nilai_bobot == -1)
                        $bobotByNim[$krteria->kode_kriteria][$data->nim]['Core Factor'][] = 4;
                    if ($data->nilai_bobot == 2)
                        $bobotByNim[$krteria->kode_kriteria][$data->nim]['Core Factor'][] = 3.5;
                    if ($data->nilai_bobot == -2)
                        $bobotByNim[$krteria->kode_kriteria][$data->nim]['Core Factor'][] = 3;
                    if ($data->nilai_bobot == 3)
                        $bobotByNim[$krteria->kode_kriteria][$data->nim]['Core Factor'][] = 2.5;
                    if ($data->nilai_bobot == -3)
                        $bobotByNim[$krteria->kode_kriteria][$data->nim]['Core Factor'][] = 2;
                    if ($data->nilai_bobot == 4)
                        $bobotByNim[$krteria->kode_kriteria][$data->nim]['Core Factor'][] = 1.4;
                    if ($data->nilai_bobot == -4)
                        $bobotByNim[$krteria->kode_kriteria][$data->nim]['Core Factor'][] = 1;
                }
            }
        }
        // var_dump($bobotByNim[4]);
        // echo "<br>"; 
        // echo array_sum($bobotByNim[1]['175410025']['Core Factor']) / count($bobotByNim[1]['175410025']['Core Factor']);

        //perhitungan nilia bobot berdasarkan kriteria
        $listMahasiswa = Mahasiswa::all();
        $nilaiBobotKriteria = [];
        foreach ($listMahasiswa as $mahasiswa) {
            foreach ($listKriteria as $krteria){
            
                $nilaiBobotKriteria[$mahasiswa->nim][$krteria->kode_kriteria] = (array_sum($bobotByNim[$krteria->kode_kriteria][$mahasiswa->nim]['Core Factor']) / count($bobotByNim[$krteria->kode_kriteria][$mahasiswa->nim]['Core Factor']) * ($krteria->persentase_core / 100)) +
                (array_sum($bobotByNim[$krteria->kode_kriteria][$mahasiswa->nim]['Secondary Factor']) / count($bobotByNim[$krteria->kode_kriteria][$mahasiswa->nim]['Secondary Factor']) * ($krteria->persentase_second / 100));
                
            }
        }

        // echo "<br>"; 
        // var_dump($nilaiBobotKriteria);
        // echo "<br>"; 

        $listHasilAkhist = [];
        $hasil = 0;
        foreach ($listMahasiswa as $mahasiswa) {
            foreach ($listKriteria as $krteria){
                // echo $nilaiBobotKriteria[$mahasiswa->nim][$krteria->kode_kriteria] * $krteria->persentase_kriteria / 100;
                // echo "<br>"; 

                $hasil += $nilaiBobotKriteria[$mahasiswa->nim][$krteria->kode_kriteria] * ($krteria->persentase_kriteria / 100);
            }
            array_push($listHasilAkhist, array(
                'nim' => $mahasiswa->nim,
                'nama' => $mahasiswa->nama,
                'hasil' => $hasil,
                'tanggal' => $mahasiswa->tanggal
            ));
            $hasil = 0;
        }
        usort($listHasilAkhist, function($a, $b) {
            return $a['hasil'] <= $b['hasil'];
        });        
        return view('perhitungan.index',compact('listHasilAkhist'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
