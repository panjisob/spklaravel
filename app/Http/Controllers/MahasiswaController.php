<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subkriteria;
use App\Models\Kriteria;
use App\Models\Mahasiswa;
use App\Models\Nilai;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $listMahasiswa = Mahasiswa::all();
        return view('mahasiswa.index',compact('listMahasiswa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listSubKriteria = Subkriteria::all()->sortBy('kode_subkriteria');
        return view('mahasiswa.create',compact('listSubKriteria'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nilai = $request->input('nilai');
        $kode = $request->input('kode_subkriteria');

        $value = [];
        $i= 0;
        foreach ($nilai as $data) {
            array_push($value, array(
                'nim' => $request->input('nim'),
                'nilai' => $data,
                'kode_subkriteria' => $kode[$i]
            ));
            $i++;
        }
        $current_time = \Carbon\Carbon::now()->toDateTimeString();

        $mahasiswa = array(
            'nim' => $request->input('nim'),
            'nama' => $request->input('nama_mahasiswa'),
            'user_id' => Auth::user()->id,
            'tanggal' => $current_time
        );
        DB::table('nilais')->insert($value);
        DB::table('mahasiswas')->insert($mahasiswa);

        return redirect()->route('mahasiswa.index')
                        ->with('success','tambah data mahasiswa berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($nim)
    {
        $data = Mahasiswa::firstOrFail()->where('nim',$nim)->get();
        $mahasiswa = $data[0];
        $nilai = DB::table('nilais')
        ->select('nilais.nim','nilais.nilai',  
            DB::raw("(select subkriterias.nama_subkriteria from subkriterias 
                    where subkriterias.kode_subkriteria = nilais.kode_subkriteria) as nama_subkriteria"))
        ->orderBy('nilais.kode_subkriteria')
        ->where('nilais.nim', $nim)
        ->get();
        return view('mahasiswa.show',compact('mahasiswa', 'nilai', 'nim'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($nim)
    {
        $data = Mahasiswa::firstOrFail()->where('nim',$nim)->get();
        $mahasiswa = $data[0];
        $nilai = DB::table('nilais')
        ->select('nilais.nim','nilais.nilai', 'nilais.kode_subkriteria', 
            DB::raw("(select subkriterias.nama_subkriteria from subkriterias 
                    where subkriterias.kode_subkriteria = nilais.kode_subkriteria) as nama_subkriteria"))
        ->orderBy('nilais.kode_subkriteria')
        ->where('nilais.nim', $nim)
        ->get();
        $listSubKriteria = Subkriteria::all();
        return view('mahasiswa.edit',compact('mahasiswa', 'nilai'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $nim)
    {
        $nilai = $request->input('nilai');
        $kode = $request->input('kode_subkriteria');

        $value = [];
        $i= 0;
        foreach ($nilai as $data) {
            DB::table('nilais')->where('nim', $nim)->where('kode_subkriteria', $kode[$i])->update(array(
                'nim' => $request->input('nim'),
                'nilai' => $data));
            $i++;
        }

        $current_time = \Carbon\Carbon::now()->toDateTimeString();

        $mahasiswa = array(
            'nim' => $request->input('nim'),
            'nama' => $request->input('nama_mahasiswa'),
            'user_id' => Auth::user()->id,
            'tanggal' => $current_time
        );
        DB::table('mahasiswas')->where('nim', $nim)->update($mahasiswa);

        return redirect()->route('mahasiswa.index')
                        ->with('success','edit data mahasiswa berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nim)
    {
        Mahasiswa::firstOrFail()->where('nim',$nim)->delete();
        Nilai::firstOrFail()->where('nim',$nim)->delete();
        return redirect()->route('mahasiswa.index')
                        ->with('success','hapus data mahasiswa berhasil');
    }

}
