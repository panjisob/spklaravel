<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kriteria;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class KriteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /// mengambil data kriteria
        $listKriteria = Kriteria::all();

        /// mengirimkan variabel $kriteria ke halaman views kriteria/index.blade.php
        /// include dengan number index
        return view('kriteria.index',compact('listKriteria'));
            // ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kriteria.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /// membuat validasi untuk title dan content wajib diisi
        $request->validate([
            'kode_kriteria' => 'required',
            'nama_kriteria' => 'required',
            'persentase_kriteria' => 'required',
            'persentase_core' => 'required',
            'persentase_second' => 'required'
        ]);
        /// insert setiap request dari form ke dalam database via model
        /// jika menggunakan metode ini, maka nama field dan nama form harus sama
        Kriteria::create($request->all());
         
        /// redirect jika sukses menyimpan data
        return redirect()->route('kriteria.index')
                        ->with('success','Kriteria created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('posts.show',$post->id) }}
        return view('kriteria.show',compact('kriteria'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($kode_kriteria)
    {
        $data = Kriteria::firstOrFail()->where('kode_kriteria',$kode_kriteria)->get();
        $kriteria = $data[0];
        return view('kriteria.edit',compact('kriteria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kode_kriteria)
    {
        $request->validate([
            'kode_kriteria' => 'required',
            'nama_kriteria' => 'required',
            'persentase_kriteria' => 'required',
            'persentase_core' => 'required',
            'persentase_second' => 'required'
        ]);

        /// mengubah data berdasarkan request dan parameter yang dikirimkan
        DB::table('kriterias')->where('kode_kriteria',$kode_kriteria)->update($request->except(['_token','_method']));
         
        /// setelah berhasil mengubah data
        return redirect()->route('kriteria.index')
                        ->with('success','Post updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode_kriteria)
    {
        /// melakukan hapus data berdasarkan parameter yang dikirimkan
        Kriteria::firstOrFail()->where('kode_kriteria',$kode_kriteria)->delete();
        return redirect()->route('kriteria.index')
                        ->with('success','Post deleted successfully');
    }
}
