<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subkriteria extends Model
{
    use HasFactory;

    protected $fillable = [
        'kode_subkriteria', 'nama_subkriteria', 'tipe', 'kode_kriteria', 'bobot'
    ];
}
