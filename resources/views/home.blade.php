@extends('template')

@section('content')
<div class="container">
    <div class="col-md-12 mt-5">
        <div class="card">
            <div class="card-header">
                <h3>Home</h3>
            </div>
            <div class="card-body">
                <h5>Selamat datang , <strong>{{ Auth::user()->name }}</strong></h5>
                <br>
                @if(Auth::user()->role == 'akadameik')
                <table class="table table-bordered">
                    <tr>
                        <th width="20px" class="text-center">Nim</th>
                        <th width="50px" class="text-center">Nama Mahasiswa</th>
                    </tr>
                    @foreach ($listMahasiswa as $mahasiswa)
                    <tr>
                        <td width="20px" class="text-center">{{$mahasiswa->nim}}</td>
                        <td width="50px" class="text-center">{{$mahasiswa->nama}}</td>
                    </tr>
                    @endforeach
                </table>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection