<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="container">
            <h5 class="text-white center">
                Sistem Pendukung Keputusan Perankingan nilai kelulusan dengan metode profile matching
            </h5>
        </div>
    </nav>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container">
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('home') }}">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('kriteria.index') }}">Kriteria</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('subkriteria.index') }}">SubKriteria</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('mahasiswa.index') }}">Mahasiswa</a>
                </li>
            </ul>
        </div>
        <span class="navbar-text">
            <a href="{{ route('logout') }}">Logout</a>
        </span>
    </div>
    </nav>
    
    <div class="container">
        @yield('content')
    </div>

    <div>
        <br><br>
    </div>

</body>
</html>