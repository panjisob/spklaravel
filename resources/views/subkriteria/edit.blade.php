@extends('template')
 
@section('content')
<div class="row mt-5 mb-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>Edit SubKriteria</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-secondary" href="{{ route('subkriteria.index') }}"> Back</a>
        </div>
    </div>
</div>
 
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
 
<form action="{{ route('subkriteria.update', $subkriteria->kode_subkriteria) }}" method="POST">
    @csrf
    @method('PUT')
 
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Kode SubKriteria:</strong>
                <input type="text" name="kode_subkriteria" class="form-control" placeholder="Kode SubKriteria" value="{{ $subkriteria->kode_subkriteria }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nama SubKriteria:</strong>
                <input type="text" name="nama_subkriteria" class="form-control" placeholder="Nama SubKriteria" value="{{ $subkriteria->nama_subkriteria }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            <strong>Tipe:</strong><br>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="tipe" id="flexRadioDefault1" value="Core Factor" 
                        {{$subkriteria->tipe = 'Core Factor' ? 'checked' : ''}} >
                    <label class="form-check-label" for="flexRadioDefault1">
                        Core Factor
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="tipe" id="flexRadioDefault2" value="Secondary Factor"
                    {{$subkriteria->tipe = 'Secondary Factor' ? 'checked' : ''}} >
                    <label class="form-check-label" for="flexRadioDefault2">
                        Secondary Factor
                    </label>
                </div>      
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Bobot Kriteria:</strong>
                <input type="text" name="bobot" class="form-control" placeholder="Bobot Kriteria" value="{{ $subkriteria->bobot }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Kode Kriteria:</strong>
                <input type="text" name="kode_kriteria" class="form-control" placeholder="Kode SubKriteria" value="{{ $subkriteria->kode_kriteria }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
 
</form>
@endsection