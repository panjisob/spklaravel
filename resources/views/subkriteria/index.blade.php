@extends('template')
 
@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>SubKriteria</h2>
            </div>
            @if(Auth::user()->role == 'admin')
            <div class="float-right">
                <a class="btn btn-success" href="{{ route('subkriteria.create') }}"> Create SubKriteria</a>
            </div>
            @endif
        </div>
    </div>
 
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif
 
    <table class="table table-bordered">
        <tr>
            <th width="20px" class="text-center">Kode SubKriteria</th>
            <th width="50px" class="text-center">Nama SubKriteria</th>
            <th width="50px" class="text-center">Tipe</th>
            <th width="50px" class="text-center">Bobot</th>
            <th width="50px" class="text-center">Kode Kriteria</th>
            @if(Auth::user()->role == 'admin')
            <th width="280px"class="text-center">Action</th>
            @endif
        </tr>
        @foreach ($listSubKriteria as $subkriteria)
        <tr>
            <td>{{ $subkriteria->kode_subkriteria }}</td>
            <td>{{ $subkriteria->nama_subkriteria }}</td>
            <td>{{ $subkriteria->tipe }}</td>
            <td>{{ $subkriteria->bobot }}</td>
            <td>{{ $subkriteria->kode_kriteria }}</td>
            @if(Auth::user()->role == 'admin')
            <td class="text-center">
                <form action="{{ route('subkriteria.destroy',$subkriteria->kode_subkriteria) }}" method="POST">
  
                    <a class="btn btn-primary btn-sm" href="{{ route('subkriteria.edit',$subkriteria->kode_subkriteria) }}">Edit</a>
 
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>
                </form>
            </td>
            @endif
        </tr>
        @endforeach
    </table>
 
 
@endsection