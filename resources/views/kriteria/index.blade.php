@extends('template')
 
@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Kriteria</h2>
            </div>
            @if(Auth::user()->role == 'admin')
            <div class="float-right">
                <a class="btn btn-success" href="{{ route('kriteria.create') }}"> Create Post</a>
            </div>
            @endif
        </div>
    </div>
 
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif
 
    <table class="table table-bordered">
        <tr>
            <th width="20px" class="text-center">Kode Kriteria</th>
            <th width="50px" class="text-center">Nama Kriteria</th>
            <th width="50px" class="text-center">persentase kriteria</th>
            <th width="50px" class="text-center">persentase core</th>
            <th width="50px" class="text-center">persentase second</th>
            @if(Auth::user()->role == 'admin')
            <th width="280px"class="text-center">Action</th>
            @endif
        </tr>
        @foreach ($listKriteria as $kriteria)
        <tr>
            <td>{{ $kriteria->kode_kriteria }}</td>
            <td>{{ $kriteria->nama_kriteria }}</td>
            <td>{{ $kriteria->persentase_kriteria }}</td>
            <td>{{ $kriteria->persentase_core }}</td>
            <td>{{ $kriteria->persentase_second }}</td>
            @if(Auth::user()->role == 'admin')
            <td class="text-center">
                <form action="{{ route('kriteria.destroy',$kriteria->kode_kriteria) }}" method="POST">
 
                    <!-- <a class="btn btn-info btn-sm" href="{{ route('kriteria.show',$kriteria->kode_kriteria) }}">Show</a> -->
 
                    <a class="btn btn-primary btn-sm" href="{{ route('kriteria.edit',$kriteria->kode_kriteria) }}">Edit</a>
 
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>
                </form>
            </td>
            @endif
        </tr>
        @endforeach
    </table>
 
 
@endsection