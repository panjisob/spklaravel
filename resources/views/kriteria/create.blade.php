@extends('template')
 
@section('content')
<div class="row mt-5 mb-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>Create New Kriteria</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-secondary" href="{{ route('kriteria.index') }}"> Back</a>
        </div>
    </div>
</div>
 
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
 
<form action="{{ route('kriteria.store') }}" method="POST">
    @csrf
 
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Kode Kriteria:</strong>
                <input type="text" name="kode_kriteria" class="form-control" placeholder="Kode Kriteria">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nama Kriteria:</strong>
                <input type="text" name="nama_kriteria" class="form-control" placeholder="Nama Kriteria">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>persentase kriteria:</strong>
                <input type="text" name="persentase_kriteria" class="form-control" placeholder="presentase Kriteria">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>persentase core</strong>
                <input type="text" name="persentase_core" class="form-control" placeholder="persentase core">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>persentase second:</strong>
                <input type="text" name="persentase_second" class="form-control" placeholder="persentase second">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
 
</form>
@endsection