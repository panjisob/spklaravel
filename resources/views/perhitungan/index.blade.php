@extends('template')
 
@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Hasil Perhitungan</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-secondary" href="{{ route('mahasiswa.index') }}"> Back</a>
            </div>
        </div>
    </div>

    

    <div class="row">
        <div class="col">
            <input type="date" class="form-control" placeholder="First date" aria-label="First date" id="start">
        </div>
        <div class="col">
            <input type="date" class="form-control" placeholder="Last date" aria-label="Last date" id="end">
        </div>
        <div class="col">
            <button class="btn btn-primary" onclick="filter();">Filter</button>
        </div>
    </div>
    <br>
    <table class="table table-bordered">
        <tr>
            <th width="20px" class="text-center">Ranking</th>
            <th width="20px" class="text-center">Nim</th>
            <th width="50px" class="text-center">Nama Mahasiswa</th>
            <th width="50px"class="text-center">Hasil Akhir</th>
        </tr>
        <?php $i = 1;?>
        @foreach ($listHasilAkhist as $mahasiswa)
        <tr id="thelist">
            <td width="20px" class="text-center">{{$i++}}</td>
            <td width="20px" class="text-center">{{$mahasiswa['nim']}}</td>
            <td width="50px" class="text-center">{{$mahasiswa['nama']}}</td>
            <td width="50px" class="text-center">{{$mahasiswa['hasil']}}</td>
                
        </tr>
        @endforeach
    </table>

    <table class="table table-bordered">
        <thead class="thead-dark">
            <tr>
                <th width="20px" class="text-center">Ranking</th>
                <th width="20px" class="text-center">Nim</th>
                <th width="50px" class="text-center">Nama Mahasiswa</th>
                <th width="50px"class="text-center">Hasil Akhir</th>
            </tr>
        </thethead>
        <tbody id="tabel">
            
        </tbody>
    </table>

    <script>

        var listData = {!! json_encode($listHasilAkhist) !!};
        show();
        function show() {
            var counter= 1;
            console.log(JSON.stringify(listData));

            listData.forEach(function(data, index) {
                console.log(data);
                document.getElementById("tabel").innerHTML += "<tr>"+
                    "<td width='20px' class='text-center'>"+counter+"</td>"+
                    "<td width='20px' class='text-center'>"+data.nim+"</td>"+
                    "<td width='50px' class='text-center'>"+data.nama+"</td>"+
                    "<td width='50px' class='text-center'>"+data.hasil.toFixed(2)+"</td>"+
                "</tr>";
                counter++;
            });


        };

        function filter() {
            document.getElementById("tabel").innerHTML = "";
            var startDate = new Date(document.getElementById("start").value);
            var endDate = new Date(document.getElementById("end").value);
            console.log(startDate);
            console.log(endDate);
            var counter= 1;

            listData.forEach(function(data, index) {
                var date = new Date(data.tanggal)
                console.log(date +" >= "+ startDate +" && "+ date +" <= "+ endDate);
                console.log(date >= startDate && date <= endDate);
                if (date >= startDate && date <= endDate){
                    document.getElementById("tabel").innerHTML += "<tr>"+
                        "<td width='20px' class='text-center'>"+counter+"</td>"+
                        "<td width='20px' class='text-center'>"+data.nim+"</td>"+
                        "<td width='50px' class='text-center'>"+data.nama+"</td>"+
                        "<td width='50px' class='text-center'>"+data.hasil.toFixed(2)+"</td>"+
                    "</tr>";
                    counter++;
                }
            });

        }
    </script>

@endsection