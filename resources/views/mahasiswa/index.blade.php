@extends('template')
 
@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Mahasiswa</h2>
            </div>
            @if(!Auth::user()->role == 'akademik')
            <div class="float-right">
                <a class="btn btn-success" href="{{ route('mahasiswa.create') }}">Tambah Data</a>
            </div>
            @endif
        </div>
    </div>
 
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif

    
    <table class="table table-bordered">
        <tr>
            <th width="20px" class="text-center">Nim</th>
            <th width="50px" class="text-center">Nama Mahasiswa</th>
            <th width="280px"class="text-center">Action</th>
        </tr>
        @foreach ($listMahasiswa as $mahasiswa)
        <tr>
                <td width="20px" class="text-center">{{$mahasiswa->nim}}</td>
                <td width="50px" class="text-center">{{$mahasiswa->nama}}</td>
                <td class="text-center">
                    <form action="{{ route('mahasiswa.destroy',$mahasiswa->nim) }}" method="POST">
    
                        <a class="btn btn-primary btn-sm" href="{{ route('mahasiswa.show',$mahasiswa->nim) }}">Lihat Nilai</a>
                        @if(!Auth::user()->role == 'akademik')
                            <a class="btn btn-success btn-sm" href="{{ route('mahasiswa.edit',$mahasiswa->nim) }}">Edit</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>
                        @endif
                    </form>
                </td>
        </tr>
        @endforeach
    </table>

    <a class="btn btn-success" href="{{ route('perhitungan.index') }}">Perhitungan Data</a>
@endsection