@extends('template')
 
@section('content')
<div class="row mt-5 mb-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>Edit Data Mahasiswa</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-secondary" href="{{ route('mahasiswa.index') }}"> Back</a>
        </div>
    </div>
</div>
 
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
 
<form action="{{ route('mahasiswa.update',$mahasiswa->nim) }}" method="POST">
    @csrf
    @method('PUT')
    
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nim :</strong>
                <input type="text" name="nim" class="form-control" placeholder="Nim" value="{{$mahasiswa->nim}}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nama Mahasiswa:</strong>
                <input type="text" name="nama_mahasiswa" class="form-control" placeholder="Nama Mahasiswa" value="{{$mahasiswa->nama}}">
            </div>
        </div>
        @foreach ($nilai as $n)
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>{{ $n->nama_subkriteria }}:</strong>
                <input type="text" name="nilai[]" class="form-control" placeholder="Nilai" value="{{$n->nilai}}">
                <input type="text" name="kode_subkriteria[]" value="{{ $n->kode_subkriteria }}" hidden>
            </div>
        </div>
        @endforeach
        
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
 
</form>
@endsection