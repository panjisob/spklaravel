@extends('template')
 
@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Nama : {{$mahasiswa->nama}}</h2>
                <h2>Nim : {{$mahasiswa->nim}}</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-success" href="{{ route('mahasiswa.edit',$nim) }}">Edit Data</a>
            </div>
        </div>
    </div>
 
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif

    
    <table class="table table-bordered">
        <tr>
            <th>Nama SubKriteria</th>
            <th>Nilai</th>
        </tr>
        @foreach ($nilai as $n)
        <tr>
            <td>{{$n->nama_subkriteria}}</td>
            <td>{{$n->nilai}}</td>
        </tr>
        @endforeach
    </table>


@endsection